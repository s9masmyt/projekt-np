/*
 * 
 */
package np2015;

import java.util.concurrent.ConcurrentHashMap;

/**
 * The Class Column.
 */
public class Column implements Runnable {

    /** The map. */
    private ConcurrentHashMap<Integer, Node> map = new ConcurrentHashMap<Integer, Node>();

    /** The right. */
    private Column left, right;

    /** The height. */
    private int column, iteration, height;

    /** The synchronize barriers. */
    private Barrier synchroBarrierStart, synchroBarrierEnd;

    /** The step barriers. */
    private Barrier stepBarrierStart, stepBarrierEnd;

    /** The ginfo. */
    private GraphInfo ginfo;

    /** The convergend. */
    private Convergend convergend;

    /** The Constant STEPS. */
    public static final int STEPS = 100;

    /**
     * Instantiates a new column.
     *
     * @param column
     *            the column
     * @param left
     *            the left
     * @param right
     *            the right
     * @param ginfo
     *            the ginfo
     * @param convergend
     *            the convergend
     * @param iteration
     *            the iteration
     * @param synchroBarrierStart
     *            the synchro barrier start
     * @param synchroBarrierEnd
     *            the synchro barrier end
     * @param stepBarrierStart
     *            the step barrier start
     * @param stepBarrierEnd
     *            the step barrier end
     */
    public Column(int column, Column left, Column right, GraphInfo ginfo,
            Convergend convergend, int iteration, Barrier synchroBarrierStart,
            Barrier synchroBarrierEnd, Barrier stepBarrierStart,
            Barrier stepBarrierEnd) {
        this.ginfo = ginfo;
        this.height = ginfo.height;
        this.iteration = iteration;
        this.column = column;
        this.convergend = convergend;
        this.setLeftColumn(left);
        this.setRightColumn(right);
        this.synchroBarrierStart = synchroBarrierStart;
        this.synchroBarrierEnd = synchroBarrierEnd;
        this.stepBarrierStart = stepBarrierStart;
        this.stepBarrierEnd = stepBarrierEnd;
        this.stepBarrierStart.increase();
        this.stepBarrierEnd.increase();
    }

    /**
     * Instantiates a new column.
     *
     * @param column
     *            the column
     * @param left
     *            the left
     * @param right
     *            the right
     * @param ginfo
     *            the ginfo
     * @param convergend
     *            the convergend
     * @param iteration
     *            the iteration
     * @param map
     *            the map
     * @param synchroBarrierStart
     *            the synchro barrier start
     * @param synchroBarrierEnd
     *            the synchro barrier end
     * @param stepBarrierStart
     *            the step barrier start
     * @param stepBarrierEnd
     *            the step barrier end
     */
    public Column(int column, Column left, Column right, GraphInfo ginfo,
            Convergend convergend, int iteration,
            ConcurrentHashMap<Integer, Node> map, Barrier synchroBarrierStart,
            Barrier synchroBarrierEnd, Barrier stepBarrierStart,
            Barrier stepBarrierEnd) {
        this.ginfo = ginfo;
        this.height = ginfo.height;
        this.iteration = iteration;
        this.column = column;
        this.convergend = convergend;
        this.setLeftColumn(left);
        this.setRightColumn(right);
        this.map = map;
        this.synchroBarrierStart = synchroBarrierStart;
        this.synchroBarrierEnd = synchroBarrierEnd;
        this.stepBarrierStart = stepBarrierStart;
        this.stepBarrierEnd = stepBarrierEnd;
        this.stepBarrierStart.increase();
        this.stepBarrierEnd.increase();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        boolean b = true;
        convergend.setNotConvergend(this.column);
        while (b) {
            this.iteration();
            this.synchronize();
            b = convergend.notConvergend();
        }
        synchroBarrierStart.deacrease();
        synchroBarrierEnd.deacrease();
        stepBarrierStart.deacrease();
        stepBarrierEnd.deacrease();
    }

    /**
     * Gets the steps.
     *
     * @return the steps
     */
    private int getSteps() {
        return STEPS - (convergend.countConvergend() * STEPS / ginfo.width);
    }

    /**
     * Calculate one iteration.
     */
    private void iteration() {
        try {
            stepBarrierStart.await();
        } catch (InterruptedException e) {
            return;
        }
        int steps = this.getSteps();
        for (int i = 0; i < steps; i++) {
            this.iterationStep();
        }
        iteration += steps;
        // try {
        // stepBarrierEnd.await();
        // } catch (InterruptedException e) {
        // return;
        // }
    }

    /**
     * Calculate one iteration step.
     */
    private void iterationStep() {
        for (int y = 0; y < height; y++) {
            Node node = map.get(y);
            if (node != null) {
                double v = node.getValue();
                if (v == 0)
                    continue;
                if (column != 0) {
                    double rate = ginfo.getRateForTarget(column, y,
                            Neighbor.Left);
                    node.addLeft(v * rate);
                    node.addAkku(-v * rate);
                }
                if (column != ginfo.width - 1) {
                    double rate = ginfo.getRateForTarget(column, y,
                            Neighbor.Right);
                    node.addRight(v * rate);
                    node.addAkku(-v * rate);
                }
                if (y != 0) {
                    double rate = ginfo.getRateForTarget(column, y,
                            Neighbor.Top);
                    double top = v * rate;
                    if (top != 0) {
                        int j = y - 1;
                        Node n = map.get(j);
                        if (n != null) {
                            n.addAkku(top);
                            node.addAkku(-top);
                        } else {
                            n = new Node(0);
                            n.addAkku(top);
                            node.addAkku(-top);
                            int i = y - 1;
                            map.putIfAbsent(i, n);
                        }
                    }
                }
                if (y != height - 1) {
                    double rate = ginfo.getRateForTarget(column, y,
                            Neighbor.Bottom);
                    double bottom = v * rate;
                    if (bottom != 0) {
                        int j = y + 1;
                        Node n = map.get(j);
                        if (n != null) {
                            n.addAkku(bottom);
                            node.addAkku(-bottom);
                        } else {
                            n = new Node(0);
                            n.addAkku(bottom);
                            node.addAkku(-bottom);
                            int i = y + 1;
                            map.putIfAbsent(i, n);
                        }
                    }
                }
            }
        }
        verticalUpdate();
    }

    /**
     * Vertical update.
     */
    private void verticalUpdate() {
        for (int y = 0; y < height; y++) {
            Node node = map.get(y);
            if (node != null) {
                node.addValue(node.getAkku());
                node.resetAkku();
                if (node.getValue() <= 0 && node.getLeftOutflow() == 0
                        && node.getRightOutflow() == 0) {
                    map.remove(y);
                }
            }
        }
    }

    /**
     * Synchronize the column with its neighbours. Creates new columns, if its
     * outflow ist greater than zero und there doesn't exists a column on this
     * site.
     */
    private void synchronize() {
        double entireLeftOutflow = 0;
        double entireRightOutflow = 0;
        double entireLeftInflow = 0;
        double entireRightInflow = 0;
        for (int y = 0; y < height; y++) {
            Node node = map.get(y);
            if (node != null) {
                entireRightOutflow += node.getRightOutflow();
                entireLeftOutflow += node.getLeftOutflow();
            }
        }
        boolean initRight = false, initLeft = false;
        if (right == null && column < ginfo.width - 1
                && entireRightOutflow > 0) {
            right = new Column(this.column + 1, this, null, this.ginfo,
                    this.convergend, this.iteration, this.synchroBarrierStart,
                    this.synchroBarrierEnd, this.stepBarrierStart,
                    this.stepBarrierEnd);
            initRight = true;
            right.initialize(this);
            Thread rightThread = new Thread(right);
            rightThread.start();
        }
        if (left == null && column > 0 && entireLeftOutflow > 0) {
            left = new Column(this.column - 1, null, this, this.ginfo,
                    this.convergend, this.iteration, this.synchroBarrierStart,
                    this.synchroBarrierEnd, this.stepBarrierStart,
                    this.stepBarrierEnd);
            initLeft = true;
            left.initialize(this);
            Thread leftThread = new Thread(left);
            leftThread.start();
        }
        boolean rightCheck = !initRight && right != null,
                leftCheck = !initLeft && left != null;
        if (rightCheck || leftCheck) {
            try {
                synchroBarrierStart.await();
            } catch (InterruptedException e) {
                return;
            }
            if (rightCheck) {
                entireRightInflow = this.synchronizeRight();
            }
            if (leftCheck) {
                entireLeftInflow = this.synchronizeLeft();
            }
            if (Math.abs(entireLeftInflow + entireRightInflow
                    - entireLeftOutflow - entireRightOutflow) < ginfo.epsilon) {
                convergend.setConvergend(column);
            } else {
                convergend.setNotConvergend(column);
            }
            try {
                synchroBarrierEnd.await();
            } catch (InterruptedException e) {
                return;
            }
        }
        if (initLeft) {
            synchroBarrierStart.increase();
            synchroBarrierEnd.increase();
        }
        if (initRight) {
            synchroBarrierStart.increase();
            synchroBarrierEnd.increase();
        }
    }

    /**
     * Initialize the column from a neihgbour column.
     *
     * @param c
     *            the Column
     */
    private void initialize(Column c) {
        for (int y = 0; y < this.height; y++) {
            Node node = c.getNode(y);
            if (node != null) {
                if (node.getRightOutflow() > 0) {
                    this.map.put(y, new Node(node.getRightOutflow()));
                }
                node.resetRightOutflow();
            }
        }
    }

    /**
     * Synchronize with the right column.
     *
     * @return the inflow of the right column
     */
    private double synchronizeRight() {
        double entireInflow = 0;
        for (int y = 0; y < height; y++) {
            Node nthis = map.get(y);
            Node nright = right.getNode(y);
            if (nthis != null) {
                if (nright != null) {
                    double inflow = nright.getLeftOutflow();
                    entireInflow += inflow;
                    nthis.addValue(inflow);
                    nright.resetLeftOutflow();
                }
            } else {
                if (nright != null) {
                    double inflow = nright.getLeftOutflow();
                    if (inflow > 0) {
                        Node n = new Node(inflow);
                        map.putIfAbsent(y, n);
                        entireInflow += inflow;
                    }
                    nright.resetLeftOutflow();
                }
            }
        }
        return entireInflow;
    }

    /**
     * Synchronize with the left column.
     *
     * @return the inflow of the left column
     */
    private double synchronizeLeft() {
        double entireInflow = 0;
        for (int y = 0; y < height; y++) {
            Node nthis = map.get(y);
            Node nleft = left.getNode(y);
            if (nthis != null) {
                if (nleft != null) {
                    double inflow = nleft.getRightOutflow();
                    entireInflow += inflow;
                    nthis.addValue(inflow);
                    nleft.resetRightOutflow();
                }
            } else {
                if (nleft != null) {
                    double inflow = nleft.getRightOutflow();
                    if (inflow > 0) {
                        Node n = new Node(inflow);
                        map.putIfAbsent(y, n);
                        entireInflow += inflow;
                    }
                    nleft.resetRightOutflow();
                }
            }
        }
        return entireInflow;
    }

    /**
     * Gets the value.
     *
     * @param row
     *            the row
     * @return the value
     */
    public double getValue(int row) {
        Node n = map.get(row);
        if (n != null)
            return n.getValue();
        return 0;
    }

    /**
     * Gets the left column.
     *
     * @return the left column
     */
    public Column getLeftColumn() {
        return left;
    }

    /**
     * Sets the left column.
     *
     * @param left
     *            the new left column
     */
    public void setLeftColumn(Column left) {
        this.left = left;
    }

    /**
     * Gets the right column.
     *
     * @return the right column
     */
    public Column getRightColumn() {
        return right;
    }

    /**
     * Sets the right column.
     *
     * @param right
     *            the new right column
     */
    public void setRightColumn(Column right) {
        this.right = right;
    }

    /**
     * Gets the column.
     *
     * @return the column
     */
    public int getColumn() {
        return column;
    }

    /**
     * Gets the node.
     *
     * @param y
     *            the y
     * @return the node
     */
    public Node getNode(int y) {
        return map.get(y);
    }

    /**
     * Gets the value.
     *
     * @param column
     *            the column
     * @param row
     *            the row
     * @return the value
     */
    public double getValue(int column, int row) {
        if (this.column == column) {
            return this.getValue(row);
        } else if (this.column < column) {
            if (right == null)
                return 0;
            return this.right.getValue(column, row);
        } else {
            if (left == null)
                return 0;
            return this.left.getValue(column, row);
        }
    }
}
