/*
 * 
 */
package np2015;

// TODO: Auto-generated Javadoc
public class Node {
    
    /** The prev. */
    private double leftakku = 0, rightakku = 0, value, akku = 0, prev = 0;

    /**
     * Instantiates a new node.
     *
     * @param value the value
     */
    public Node(double value) {
        this.value = value;
    }

    /**
     * Gets the left outflow.
     *
     * @return the left outflow
     */
    public synchronized double getLeftOutflow() {
        return leftakku;
    }

    /**
     * Gets the right outflow.
     *
     * @return the right outflow
     */
    public synchronized double getRightOutflow() {
        return rightakku;
    }

    /**
     * Adds the left.
     *
     * @param v the v
     */
    public synchronized void addLeft(double v) {
        leftakku += v;
    }

    /**
     * Adds the right.
     *
     * @param v the v
     */
    public synchronized void addRight(double v) {
        rightakku += v;
    }

    /**
     * Adds the akku.
     *
     * @param v the v
     */
    public synchronized void addAkku(double v) {
        akku += v;
    }

    /**
     * Reset left outflow.
     */
    public synchronized void resetLeftOutflow() {
        leftakku = 0;
    }

    /**
     * Reset right outflow.
     */
    public synchronized void resetRightOutflow() {
        rightakku = 0;
    }

    /**
     * Adds the value.
     *
     * @param value the value
     */
    public synchronized void addValue(double value) {
        this.prev = this.value;
        this.value += value;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public synchronized double getValue() {
        return value;
    }

    /**
     * Gets the akku.
     *
     * @return the akku
     */
    public synchronized double getAkku() {
        return akku;
    }

    /**
     * Reset akku.
     */
    public synchronized void resetAkku() {
        akku = 0;
    }

    /**
     * Checks if is convergend.
     *
     * @param epsilon the epsilon
     * @return true, if is convergend
     */
    public synchronized boolean isConvergend(double epsilon) {
        return Math.abs(this.value - this.prev) < epsilon;
    }

}
