/*
 * 
 */
package np2015;

/**
 * The Class Barrier.
 */
public class Barrier {

    /** The count of the requiered threads in this barrier. */
    private int count = 0;

    /** The current threads in this barrier. */
    private int current = 0;

    /**
     * Instantiates a new barrier.
     *
     * @param count
     *            the count
     */
    public Barrier(int count) {
        this.count = count;
    }

    /**
     * Increase the count.
     */
    public synchronized void increase() {
        count++;
    }

    /**
     * Deacrease the count.
     */
    public synchronized void deacrease() {
        count = count == 0 ? 0 : count - 1;
        if (count <= current) {
            this.notifyAll();
        }
    }

    /**
     * Threads entered this method and wait until there are enough threads.
     * After this each thread is notifed.
     *
     * @throws InterruptedException
     *             the interrupted exception
     */
    public synchronized void await() throws InterruptedException {
        current++;
        if (current < count) {
            this.wait();
        }
        this.notifyAll();
        current = 0;
    }
}
