/*
 * 
 */
package np2015;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

// TODO: Auto-generated Javadoc
/**
 * The Class Concurrent.
 */
public class Concurrent implements ImageConvertible {

    /** The column. */
    private Column column;

    /**
     * Instantiates a new concurrent.
     *
     * @param ginfo
     *            the ginfo
     */
    public Concurrent(GraphInfo ginfo) {
        if (ginfo.width > 0) {
            Convergend con = new Convergend(ginfo.width);
            int col = ginfo.column2row2initialValue.keySet()
                    .toArray(new Integer[1])[0];
            HashMap<Integer, Double> m = ginfo.column2row2initialValue.get(col);
            int row = m.keySet().toArray(new Integer[1])[0];
            double s = m.get(row);
            ConcurrentHashMap<Integer, Node> map = new ConcurrentHashMap<Integer, Node>();
            map.put(row, new Node(s));
            Barrier synchroBarrierStart = new Barrier(1);
            Barrier synchroBarrierEnd = new Barrier(1);
            Barrier stepBarrierStart = new Barrier(0);
            Barrier stepBarrierEnd = new Barrier(0);
            column = new Column(col, null, null, ginfo, con, 0, map,
                    synchroBarrierStart, synchroBarrierEnd, stepBarrierStart,
                    stepBarrierEnd);
            column.run();
        }
    }

    @Override
    public double getValueAt(int column, int row) {
        return this.column.getValue(column, row);
    }
}
