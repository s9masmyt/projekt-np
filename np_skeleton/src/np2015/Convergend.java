/*
 * 
 */
package np2015;

public class Convergend {

    /** The convergend. */
    private Boolean[] lokalConvergend;

    /**
     * Instantiates a new convergend.
     *
     * @param width
     *            the width
     */
    public Convergend(int width) {
        lokalConvergend = new Boolean[width];
    }

    /**
     * Not convergend.
     *
     * @return true, if successful
     */
    public synchronized boolean notConvergend() {
        boolean b = true;
        for (int i = 0; i < lokalConvergend.length && b; i++) {
            if (lokalConvergend[i] != null)
                b &= lokalConvergend[i];
        }
        return !b;
    }

    /**
     * Checks for convergend.
     *
     * @return the int
     */
    public synchronized int countConvergend() {
        int count = 0;
        for (int i = 0; i < lokalConvergend.length; i++) {
            if (lokalConvergend[i] != null) {
                if (lokalConvergend[i]) {
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * Sets the convergend.
     *
     * @param column
     *            the new convergend
     */
    public synchronized void setConvergend(int column) {
        lokalConvergend[column] = true;
    }

    /**
     * Sets the not convergend.
     *
     * @param column
     *            the new not convergend
     */
    public synchronized void setNotConvergend(int column) {
        lokalConvergend[column] = false;
    }
}
