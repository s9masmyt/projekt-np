package np2015;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.gson.Gson;

public class NPOsmose {

    public static void main(String[] args)
            throws IOException, InterruptedException {
    	long timeBegin = System.currentTimeMillis();
        Gson gson = new Gson();
        String json = "";
        // read data in
        if (args.length != 0) {
            Path path = Paths.get(args[0]);
            System.out.println(path);
            try {
                json = new String(Files.readAllBytes(path));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.err.println(
                    "You must provide the serialized file as the first argument!");
        }
        GraphInfo ginfo = gson.fromJson(json, GraphInfo.class);
        // Your implementation can now access ginfo to read out all important
        // values
        ImageConvertible graph = new Concurrent(ginfo); // <--- you should
                                                        // implement
                                                        // ImageConvertible to
                                                        // write the graph out
        long timeEnd = System.currentTimeMillis();
        System.out.println((timeEnd - timeBegin)/1000);
        ginfo.write2File("./result.txt", graph);
    }

}
